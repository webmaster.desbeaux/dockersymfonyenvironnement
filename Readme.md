
DockerSymfonyEnvironnement
==
Info : Pour Linux


Permet de creer des environnements de developpement dans symfony 2/3 et 4.
Ces environnements tournent sous docker.
En fonction de la version ou bien si le projet est déja existant, les configurations ne sont pas les mêmes.


01_Final_sf2_base_old
-
Premier test, à ne pas utiliser


02_Final_sf2_projet_existant
-
Récupère les données pour compléter le projet :
- composer install
- composer update
- npm install
- gulp
- Création des repertoires de logs et cache et modification des droits

03_Final_sf4_projet_existant
-
Récupère les données pour compléter le projet :
- composer install
- composer update
- npm install
- yarn
- webpack encore
- Création des repertoires de logs et cache et modification des droits

04_Final_sf4_projet_nouveau
-
Créer un nouveau projet web symfony :
- Récupération Symfony + apache pack
- Mise en place de yarn et webpack encore (pas encore en place)
- Création des repertoires de logs et cache et modification des droits

Utilisation :
Création d'un nouveau projet pour Symfony 4 :
```shell
    git@gitlab.com:webmaster.desbeaux/dockersymfonyenvironnement.git
    cd dockersymfonyenvironnement
    make install NAME=NomDuProjet
    cd NomDuProjet
    docker-compose up
```

05_Final_sf2_projet_existant_https
-
Même que 02_Final_sf2_projet_existant mais avec le HTTPS.
Montage des certificats dans le docker de apache et ajout du port 9996 pour accèder au site

06_Final_sf4_projet_existant_https
-
Même que 03_Final_sf4_projet_existant mais avec le HTTPS.
Montage des certificats dans le docker de apache et ajout du port 9996 pour accèder au site


Configuration Docker Final :
-
- php7
- apache : Port 9999; Port 9996 pour HTTPS
- mysql 5.6.40
- phpmyadmin : Port 9998
- maildev : Port 9997

Requiers
-
Commun :
- Docker
- Docker-compose
- NodeJS
- Npm
- Composer

Pour Symfony 2/3 :
- Gulp

Pour Symfony 4 :
- Yarn




Informations complémentaires :
Dans app_dev.php ajout de l'adresse IP 
    '172.23.0.1', //Docker
afin de pouvoir se mettre en dev