# apache php mysql

taken from [here](https://www.cloudreach.com/blog/containerize-this-php-apache-mysql-within-docker-containers/)

Mise a jour de ce templates pour démarrer un docker.

Contient:
- Apache
    Redirection dans ./web
    Modules:
    - mod_proxy_fcgi
    - mod_rewrite
    - mod_proxy
    - mod_deflate

ssl_module modules/mod_ssl.so
socache_shmcb_module modules/mod_socache_shmcb.so


    Options:
    - LogLevel rewrite:trace3

- PHP 7
    Extensions:
    - Imagick
    - pdo_mysql
    
- Mysql

- Maildev